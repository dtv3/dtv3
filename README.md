Download videos from TV3

Installation
============

	sudo aptitude install jshon xmlstarlet
	sudo wget -O /usr/local/bin/dtv3 https://gitlab.com/dtv3/dtv3/raw/master/dtv3
	sudo chmod +x /usr/local/bin/dtv3

Usage
=====

Run dtv3 to show the available enviroment variables to control the program and some examples. Or read the firsts lines of the script.
